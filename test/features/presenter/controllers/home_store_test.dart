import 'package:clean_architecture/core/errors/failures.dart';
import 'package:clean_architecture/features/domain/usecases/get_space_media_from_date_usecase.dart';
import 'package:clean_architecture/features/presenter/controllers/home_controller.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks/date_mock.dart';
import '../../../mocks/space_media_entity_mock.dart';

class MockGetSpaceMediaFromDateUsecase extends Mock
    implements GetSpaceMediaFromDateUsecase {}

void main() {
  late HomeController homeController;
  late GetSpaceMediaFromDateUsecase mockUsecase;

  setUp(() {
    mockUsecase = MockGetSpaceMediaFromDateUsecase();
    homeController = HomeController(mockUsecase);
  });

  test('should return a SpaceMedia from the usecase', () async {
    when(() => mockUsecase(any())).thenAnswer(
      (_) async => const Right(tSpaceMedia),
    );

    await homeController.getSpaceMediaFromDate(tDate);
    homeController.spaceMediaData;
    expect(homeController.spaceMediaData.value, tSpaceMedia);
  });

  final tFailure = ServerFailure();

  test('should return a Failure from the usecase when there is an error',
      () async {
    when(() => mockUsecase(any())).thenAnswer((_) async => Left(tFailure));

    await homeController.getSpaceMediaFromDate(tDate);
    expect(homeController.spaceMediaError.value, tFailure);
    // homeController.observer(
    //   onError: (error) {
    //     expect(error, tFailure);
    //     verify(() => mockUsecase(tDate)).called(1);
    //   },
    // );
  });
}
