import 'dart:convert';

import 'package:clean_architecture/features/data/models/space_media_model.dart';
import 'package:clean_architecture/features/domain/entities/space_media_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../mocks/space_media_mock.dart';

void main() {
  const tSpaceMediaModel = SpaceMediaModel(
    description: 'Meteors can be colorful.',
    mediaType: 'image',
    title: 'A Colorful Quadrantid Meteor',
    mediaUrl:
        'https://apod.nasa.gov/apod/image/2102/MeteorStreak_Kuszaj_1080.jpg',
  );

  test('should be a subclass of SpaceMediaEntity', () {
    expect(tSpaceMediaModel, isA<SpaceMediaEntity>());
  });

  test('should return a valid model', () {
    final Map<String, dynamic> jsonMap = jsonDecode(spaceMediaMock);
    final result = SpaceMediaModel.fromJson(jsonMap);
    expect(result, tSpaceMediaModel);
  });

  test('should return a valid json map', () {
    final expectedMap = {
      "explanation": "Meteors can be colorful.",
      "media_type": "image",
      "title": "A Colorful Quadrantid Meteor",
      "url":
          "https://apod.nasa.gov/apod/image/2102/MeteorStreak_Kuszaj_1080.jpg",
    };

    final result = tSpaceMediaModel.toJson();
    expect(result, expectedMap);
  });
}
