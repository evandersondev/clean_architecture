import 'package:clean_architecture/core/errors/exceptions.dart';
import 'package:clean_architecture/core/http_client/http_client.dart';
import 'package:clean_architecture/features/data/datasources/space_media_datasource.dart';
import 'package:clean_architecture/features/data/datasources/space_media_datasource_implemantation.dart';
import 'package:clean_architecture/features/data/models/space_media_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../mocks/space_media_mock.dart';

class HttpClientMocking extends Mock implements HttpClient {}

void main() {
  late ISpaceMediaDatasource datasource;
  late HttpClient client;

  setUp(() {
    client = HttpClientMocking();
    datasource = NasaDatasourceImplemantation(client);
  });

  final tDate = DateTime(2022, 04, 23);
  const urlExpected =
      'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&date=2022-04-23';

  void successMock() {
    when(() => client.get(any())).thenAnswer(
      (_) async => HttpResponse(
        data: spaceMediaMock,
        statusCode: 200,
      ),
    );
  }

  test('should call the get method with correct url', () async {
    successMock();

    await datasource.getSpaceMediaFromDate(tDate);
    verify(() => client.get(urlExpected)).called(1);
  });

  test('should return a SpaceMediaModel when is successful', () async {
    successMock();
    const tSpaceMediaModelExpected = SpaceMediaModel(
      description: 'Meteors can be colorful.',
      mediaType: 'image',
      title: 'A Colorful Quadrantid Meteor',
      mediaUrl:
          'https://apod.nasa.gov/apod/image/2102/MeteorStreak_Kuszaj_1080.jpg',
    );

    final result = await datasource.getSpaceMediaFromDate(tDate);
    expect(result, tSpaceMediaModelExpected);
  });

  test('should throw a ServerException when the call is unsuccessful',
      () async {
    when(() => client.get(any())).thenAnswer(
      (_) async => HttpResponse(
        data: 'Something is wrong',
        statusCode: 400,
      ),
    );

    final result = datasource.getSpaceMediaFromDate(tDate);
    expect(() => result, throwsA(ServerException()));
  });
}
