import 'package:clean_architecture/features/domain/entities/space_media_entity.dart';

const tSpaceMedia = SpaceMediaEntity(
  description: 'Any description',
  mediaType: 'image',
  title: 'A Colorful Quadrantid Meteor',
  mediaUrl:
      'https://apod.nasa.gov/apod/image/2102/MeteorStreak_Kuszaj_1080.jpg',
);
