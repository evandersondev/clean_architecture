const spaceMediaMock = """ {
  "copyright": "Frank Kuszaj",
  "date": "2021-02-02",
  "explanation": "Meteors can be colorful.",
  "hdurl": "https://apod.nasa.gov/apod/image/2102/MeteorStreak_Kuszaj_5341.jpg",
  "media_type": "image",
  "service_version": "vl",
  "title": "A Colorful Quadrantid Meteor",
  "url": "https://apod.nasa.gov/apod/image/2102/MeteorStreak_Kuszaj_1080.jpg"
}
""";
