import 'package:clean_architecture/core/http_client/http_client.dart';
import 'package:clean_architecture/core/http_client/http_implemantation.dart';
import 'package:clean_architecture/features/data/datasources/space_media_datasource.dart';
import 'package:clean_architecture/features/data/datasources/space_media_datasource_implemantation.dart';
import 'package:clean_architecture/features/data/repositories/space_media_repository_implementation.dart';
import 'package:clean_architecture/features/domain/repositories/space_media_repository.dart';
import 'package:clean_architecture/features/domain/usecases/get_space_media_from_date_usecase.dart';
import 'package:clean_architecture/features/presenter/controllers/home_controller.dart';
import 'package:get_it/get_it.dart';

class Inject {
  static void init() {
    GetIt getIt = GetIt.instance;
    // factory gera uma nova instancia todo vez q for utilizado
    // singleton utiliza a mesma instancia do inicio ao fim ou ate limpar

    // api/client
    getIt.registerLazySingleton<HttpClient>(
      () => HttpImplematation(),
    );

    // datasources
    getIt.registerLazySingleton<ISpaceMediaDatasource>(
      () => NasaDatasourceImplemantation(getIt()),
    );

    // repositories
    getIt.registerLazySingleton<ISpaceMediaRepository>(
      () => SpaceMediaRepositoryImplementation(getIt()),
    );

    // usecases
    getIt.registerLazySingleton<GetSpaceMediaFromDateUsecase>(
      () => GetSpaceMediaFromDateUsecase(getIt()),
    );

    // controller
    getIt.registerFactory<HomeController>(
      () => HomeController(getIt()),
    );
  }
}
