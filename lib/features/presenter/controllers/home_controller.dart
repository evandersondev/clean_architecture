import 'package:clean_architecture/core/errors/failures.dart';
import 'package:clean_architecture/features/domain/entities/space_media_entity.dart';
import 'package:clean_architecture/features/domain/usecases/get_space_media_from_date_usecase.dart';
import 'package:flutter/cupertino.dart';

class HomeController {
  HomeController(this.usecase);

  final GetSpaceMediaFromDateUsecase usecase;
  final loading = ValueNotifier<bool>(false);
  final spaceMediaError = ValueNotifier<Failure?>(null);
  final spaceMediaData = ValueNotifier<SpaceMediaEntity>(const SpaceMediaEntity(
    description: '',
    mediaType: '',
    mediaUrl: '',
    title: '',
  ));

  getSpaceMediaFromDate(DateTime? date) async {
    loading.value = true;

    final result = await usecase(date);

    result.fold(
      (error) => spaceMediaError.value = error,
      (success) => spaceMediaData.value = success,
    );

    loading.value = false;
  }
}
