import 'package:clean_architecture/features/presenter/widgets/custom_app_bar.dart';
import 'package:clean_architecture/features/presenter/widgets/description_bottom_sheet.dart';
import 'package:clean_architecture/features/presenter/widgets/image_network_with_loader.dart';
import 'package:flutter/material.dart';

class MediaPage extends StatelessWidget {
  const MediaPage({
    Key? key,
    required this.url,
    required this.title,
    required this.description,
  }) : super(key: key);

  final String url;
  final String title;
  final String description;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(title: 'Media details'),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.remove_red_eye),
        onPressed: () => showDescriptionBottomSheet(
          context: context,
          title: title,
          description: description,
        ),
      ),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: ImageNetworkWithLoader(url: url),
      ),
    );
  }
}
