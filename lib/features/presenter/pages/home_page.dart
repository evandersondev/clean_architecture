import 'package:clean_architecture/features/presenter/controllers/home_controller.dart';
import 'package:clean_architecture/features/presenter/pages/media_page.dart';
import 'package:clean_architecture/features/presenter/widgets/custom_app_bar.dart';
import 'package:clean_architecture/features/presenter/widgets/round_button.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var homeController = GetIt.I.get<HomeController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppBar(title: 'APOD'),
      body: AnimatedBuilder(
          animation: Listenable.merge(
              [homeController.loading, homeController.spaceMediaData]),
          builder: (_, __) {
            var loading = homeController.loading.value;
            var spaceMediaData = homeController.spaceMediaData.value;

            return loading
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : SafeArea(
                    child: Padding(
                      padding: const EdgeInsets.all(20),
                      child: Center(
                        child: Column(
                          children: [
                            Text(
                              'Welcome to Astronomy Picture of the Day!',
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .displaySmall
                                  ?.copyWith(
                                    color: Colors.black87,
                                  ),
                            ),
                            const SizedBox(height: 150),
                            RoundButton(
                              label: 'SELECT DATE',
                              onTap: () async {
                                var datePicked = await showDatePicker(
                                  context: context,
                                  helpText: 'SELECT A DATE',
                                  initialDate: DateTime.now(),
                                  firstDate: DateTime(1995, 06, 16),
                                  lastDate: DateTime.now(),
                                );

                                await homeController
                                    .getSpaceMediaFromDate(datePicked);

                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) {
                                      return MediaPage(
                                        url: spaceMediaData.mediaUrl,
                                        title: spaceMediaData.title,
                                        description: spaceMediaData.description,
                                      );
                                    },
                                  ),
                                );
                              },
                            ),
                            const SizedBox(height: 20),
                          ],
                        ),
                      ),
                    ),
                  );
          }),
    );
  }
}
