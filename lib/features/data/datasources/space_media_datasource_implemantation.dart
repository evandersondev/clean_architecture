import 'dart:convert';

import 'package:clean_architecture/core/errors/exceptions.dart';
import 'package:clean_architecture/core/utils/converters/date_to_string_converter.dart';
import 'package:clean_architecture/features/data/datasources/endpoints/nasa_endpoint.dart';
import 'package:clean_architecture/features/data/datasources/space_media_datasource.dart';
import 'package:clean_architecture/features/data/models/space_media_model.dart';

import '../../../core/http_client/http_client.dart';
import '../../../core/utils/keys/nasa_api_key.dart';

class NasaDatasourceImplemantation implements ISpaceMediaDatasource {
  final HttpClient client;

  NasaDatasourceImplemantation(this.client);

  @override
  Future<SpaceMediaModel> getSpaceMediaFromDate(DateTime date) async {
    final response = await client.get(NasaEndpoint.apod(
        NasaApiKey.apiKey, DateToStringConverter.convert(date)));

    if (response.statusCode == 200) {
      return SpaceMediaModel.fromJson(jsonDecode(response.data));
    } else {
      throw ServerException();
    }
  }
}
