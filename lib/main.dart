import 'package:clean_architecture/app_widget.dart';
import 'package:clean_architecture/core/inject/inject.dart';
import 'package:flutter/material.dart';

void main() {
  Inject.init();

  runApp(const AppWidget());
}
